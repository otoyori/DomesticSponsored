# Grand Company Leveling

This profile set is designed to take your Grand Company from Private Third Class to Second Lieutenant Rank completely automated. Second Lieutenant is when you unlock Squadrons and those can't be done by the bot. You can find more information on squadrons here https://ffxiv.consolegameswiki.com/wiki/Adventurer_Squadrons. 

The profile set will start off doing the hunt log for your grand company to obtain seals and if you're still lacking seals after that, it'll farm FATEs to get the required seals. I recommmend Azim Steppe for seal farming as it has a lot of easily completed FATEs in a small area. However, if you haven't completed the MSQ `Here There Be Xaela` and unlocked flying in Azim Steppe or you character is under 60 the profile will use Coerthas Central Highlands instead. You want to be level 50 to use Coerthas reliably. If you run the profile on a character lower than level 50 it will use the starting zone for whatever GrandCompany you're in. I.E. Mealstrom > Western La, Twin Adder > Central Shroud, Immortal Flames > Central Thanalan. The seals granted from FATEs in these low level zones suck, but it's good for low level characters.

## Setup

After unlocking the ability to do Expert Deliveries, this profile will use Dungeon Farming to get the drops and turn them in for seals. This method is much faster than FATE farming. The bot does this by turning in any items in your inventory that are available for Expert Delivery turn in. Any items, not just the dungeon drops. As such, make sure you have moved any important items out of your inventory. It only takes items from your active inventory, not Armory or Chocobo Satchel, or items stored on retainers so feel free to place your important items in any of those locations. Once you're confident your inventory is important-item-free open the Grand Company Rank UP.xml and change the `<!ENTITY AcceptRisk "0">` to 1.

I also recomend using my PandaPlugins set found here: https://github.com/domesticwarlord86/PandaPlugins. Mainly for the plugins Osiris, which will prevent the bot from crapping out when you die and Vulcan, which will use a Mender when your gear is low as well as Gluttony for eating food. You can also use the Extractor plugin to extract materia from fully spirit bound items.

You will also need the latest version of LlamaLibrary installed in your botbases folder which can be found here https://github.com/nt153133/__LlamaLibrary
(Make sure you update before running the profiles, Kayla is constantly making new tags for this set and you'll need the absolute latest.)

The majority of my profiles use Lisbeth for navigation, including this one. Saga's new Sextant 2.0 is far suprior to anything else RebornBuddy has to offer and makes for a much smoother questing experience. As such, you will need Lisbeth to make these profiles work. If you don't have it, you can find it here: https://www.siune.io/

## Startup

Once you have the required setup and have changed the `AcceptRisk` entity, switch your active botbase to OrderBot, click Load Profile and load `Grand Company Rank UP.xml`. The profile will automatically detect which rank you are and take the appropiate steps to continue the rank up journy. There's no need to manually load any of the profiles in the Dungeons, Hunts, or Quests folders.

## Known Issues

